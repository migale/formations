'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Séquence
sequence = "ATGACTGTAGCTATCGTACGTATGCGTTAA"

# Longueur de la séquence
length = len(sequence)

# Initialisation de variables
nb = 0  # numéro de codon
j = 0   # position du début de mon codon

# Boucle
while length / 3 > 0 :

    nb = nb + 1

    codon = sequence[j:j+3]

    print("Voici le codon numéro " + str(nb) + " : " + codon)

    length = length - 3

    j = j + 3
