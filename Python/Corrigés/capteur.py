'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

def read_tmp (t):
        if t > 20:
                raise exception("TMP ERROR")
        else:
                return t

# programme principal
list_tmp=(10,5,7,23,6,18,21,19,23)

nb_tmp_error = 0
sum_tmp = 0

for tmp in list_tmp:
        try:
                read_tmp(tmp)
        except Exception:
		# Version raccourcie : nb_tmp_error += 1
                nb_tmp_error = nb_tmp_error + 1
        else:
		# Version raccourcie : sum_tmp += tmp
                sum_tmp = sum_tmp + tmp

nb_tmp_lu = len(list_tmp) - nb_tmp_error

print ("Nb error tmp: ",nb_tmp_error)
print ("Mean tmp:", sum_tmp/nb_tmp_lu)
