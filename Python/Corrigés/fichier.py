'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Ouverture en lecture et en écriture
with open("../Data/fichier.txt", "r") as txt, open("../Data/fichier.fasta", "w") as fasta:

	# Parcours du fichier
	for line in txt:

		lineClean = line.strip()
		liste = lineClean.split("\t")

		fasta.write(">"+liste[0]+"\t"+liste[2]+".."+liste[3]+"\t"+liste[4]+"\n"+liste[1]+"\n")
