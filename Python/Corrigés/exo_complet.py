import re
from Bio.Seq import Seq

# Initialisation d'un dictionnaire (stockage des chromosomes)
chrom = {}

# Ouverture en lecture du fichier multiFASTA
with open("Data/S_cerevisiae_chromosomes.fna", "r") as fasta:

        # Parcours du fichier multiFASTA
        for line in fasta:

                # Retrait des caractères blancs (saut de ligne ici)
                ligne = line.strip()

                # Si la ligne commence par un >
                if ligne.startswith(">"):

                        # Recuperation de l'ID
                        # Via une expression reguliere et un groupe
                        #res = re.search("^>([A-Z0-9\.]+)\s", ligne)
                        #print("Res : ",res.group(1))
                        # Via un replace + un split
                        ligne = ligne.replace(">", "")
                        liste = ligne.split()
                        print("Liste :", liste[0])
                        # Creation d'une entrée dans le dictionnaire
                        chrom[liste[0]] = ""

                # Su la ligne ne commence pas par un > -> sequence
                else:

                        # Ajout de la sequence a partir de la clé (nom du chromosome)
                        chrom[liste[0]] += ligne

# Ouverture en lecture du fichier GFF et en écriture d'un fichier FASTA
with open("Data/S_cerevisiae_annotations.gff", "r") as gff, open("Data/S_cerevisiae_cds.fa", "w") as fasta:

        # Parcours du fichier GFF
        for line in gff:

                # Retrait des caractères blancs (saut de ligne ici)
                ligne = line.strip()

                # Passage en liste via le séparateur commun (tabulation)
                liste = ligne.split("\t")

                # Si la ligne contient 9 colonnes et qu'elle contient les infos liées à un CDS
                if len(liste) == 9 and liste[2] == "CDS":
                        # Sequence de référence
                        print(liste[0])
                        # Type Feature
                        print(liste[2])
                        # Start
                        print(liste[3])
                        # Stop
                        print(liste[4])
                        # Strand
                        print(liste[6])
                        # ID CDS Via split
                        print(liste[8].split(";")[0].split("=")[1])
                        # ID CDS Via Exp. Reguliere
                        #name = re.search("^ID=(cds[0-9]+);", liste[8])
                        #print(name.group(1))

                        # Recuperation sequence CDS dans le dictionnaire via la clé (séquence de référence = nom chromosome)
                        seq = chrom[liste[0]][int(liste[3])-1:int(liste[4])]
                        # Brin -
                        if liste[6] == "-":
                                # Reverse Complement via BioPython
                                seqBio = Seq(seq)
                                seqBio = seqBio.reverse_complement()
                                print("SeqBio", seqBio)
                                # Ecriture FASTA Sortie
                                fasta.write(">"+liste[8].split(";")[0].split("=")[1]+"\n"+str(seqBio)+"\n")
                        # Brin +
                        else:
                                # Ecriture FASTA Sortie
                                fasta.write(">"+liste[8].split(";")[0].split("=")[1]+"\n"+seq+"\n")
                        print("-"*20)
