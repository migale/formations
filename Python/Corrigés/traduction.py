'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

def traduction(sequence):

    # Dictionnaire contenant le code genetique
    code_genetique = {
                      "GCT":"A","GCC":"A","GCA":"A","GCG":"A",
                      "TTA":"L","TTG":"L","CTT":"L","CTC":"L",
                      "CTA":"L","CTG":"L","CGT":"R","CGC":"R",
                      "CGA":"R","CGG":"R","AGA":"R","AGG":"R",
                      "AAA":"K","AAG":"K","AAT":"N","AAC":"N",
                      "ATG":"M","GAT":"D","GAC":"D","TTT":"F",
                      "TTC":"F","TGT":"C","TGC":"C","CCT":"P",
                      "CCC":"P","CCA":"P","CCG":"P","CAA":"Q",
                      "CAG":"Q","TCT":"S","TCC":"S","TCA":"S",
                      "TCG":"S","AGT":"S","AGC":"S","GAA":"E",
                      "GAG":"E","ACT":"T","ACC":"T","ACA":"T",
                      "ACG":"T","GGT":"G","GGC":"G","GGA":"G",
                      "GGG":"G","TGG":"W","CAT":"H","CAC":"H",
                      "TAT":"Y","TAC":"Y","ATT":"I","ATC":"I",
                      "ATA":"I","GTT":"V","GTC":"V","GTA":"V",
                      "GTG":"V","TAG":"*","TGA":"*","TAA":"*"}

    length = len(sequence)
    print ("Longueur Seq : ", length)

    protein = ""

    for i in range(0, length, 3):

        codon = sequence[i:i+3]

        if ( len(codon) == 3 ):

            protein += code_genetique[sequence[i:i+3]]

        else:

            print ("Ma sequence n'est pas complete !")

    return protein

sequence = "ATGACCATGATTACGAATTCCCGGGGATCCGTCGACCTGCAGC"
print (traduction(sequence))
