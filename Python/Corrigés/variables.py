'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Sequence initiale
sequence = "atGCCcAAaaACcTgGAtAA"
print("Sequence initiale : ", sequence)

# Sequence en majuscules
sequpper = sequence.upper()
print("Sequence en maj : ", sequpper)

# Longueur de la sequence
length = len(sequpper)
print("Longueur : ", length)

# Nombre de codons (mots de 3)
nb_codon = (length - (length%3)) / 3
nb_codon1 = int(length / 3)
print("Nb codons : ", nb_codon)
print("Nb codons : ", nb_codon1)

# 1er codon
codon1 = sequpper[0:3]
print("1er codon : ", codon1)

# Dernier codon
#codont = sequpper[-3:]
codont = sequpper[(length-(length%3)-3):(length-(length%3))]
print("Dernier codon : ", codont)
