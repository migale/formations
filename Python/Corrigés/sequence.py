'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

"""Un module pour la manipulation de sequences"""

def majuscule (seq):
        """Retourne la séquence en majuscule"""
        return seq.upper()

def minuscule (seq):
        """Retourne la séquence en minuscule"""
        return seq.lower(seq)

def longueur (seq):
        """Retourne la longueur de la séquence"""
        return len(seq)

def premier_codon(seq):
        """Retourne le premier codon"""
        return seq[0:3]

def dernier_codon(seq):
        """Retourne le dernier codon"""
        return seq[(length-(length%3)-3):(length-(length%3))]

def reverse(seq) :
        """Retourne la sequence reverse"""
        rev = ""
        for i in range(len(seq)):
                rev += seq[len(seq)-1-i]
        return rev

def complement(seq) :
        """Retourne la sequence complémentée"""
        comp = ""
        for nucleotide in seq :
                nucleotides = {"A" : "T", "T" : "A", "G" : "C", "C" : "G"}
                comp += nucleotides[nucleotide]
        return comp
