'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Import du module re
import re

# Ouverture en lecture et en écriture
with open("../Data/Arabido.gb", "r") as genbank, open("../Data/Arabido.txt", "w") as txt:

	# Définition de motifs compilés
	motifTypeLine = re.compile("^\ {5}(?P<type>\S+)\s+(?P<strand>[a-z\(]+)?(?P<start>[0-9]+)\.+(?P<stop>[0-9]+)");
	motifQualLine = re.compile("^\ +\/(?P<type>\w+)=\"?(?P<val>\w+)\"?")

	# Ecriture de l'en-tête de notre fichier texte
	txt.write("Type\tStart\tStop\tStrand\tName\tLength\n")

	# Parcours du fichier GenBank
	for line in genbank:

		# Recherche du 1er motif compilé
		result = motifTypeLine.search(line)

		# Si le motif est trouvé, on stocke les informations
		if result != None:
			featType = result.group('type')
			start = result.group('start')
			stop = result.group('stop')
			if result.group('strand') != None:
				strand = "-1"
			else: 
				strand = "1"

		# Recherche du 2ème motif compilé
		result = motifQualLine.search(line)

		# Si le motif est trouvé, on stocke les informations et on écrit dans le fichier de sortie
		if result != None:
			if result.group('type') == 'length':
				length = result.group('val')
			elif result.group('type') == 'name':
				name = result.group('val')
				txt.write(featType+"\t"+start+"\t"+stop+"\t"+strand+"\t"+name+"\t"+length+"\n")
