'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Import du module SeqIO de Biopython
from Bio import SeqIO

# Parcours du fichier GenBank
for seq_record in SeqIO.parse("../Data/ls_orchid.gbk", "genbank"):

	# Affichage du nom de la séquence
	print(seq_record.id)
	# Affichage de la représentation de la séquence
	print(repr(seq_record.seq))
	# Affichage de la longueur de la séquence
	print(len(seq_record))
