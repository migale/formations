'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

import numpy as np

days=["Lun","Mar","Mer","Jeu","Ven","Sam","Dim"]
mean_temp=[]


temp=np.array([[12,11,14,12],[12,10,14,11],[11,11,14,13],[18,23,23,17],[17,22,21,17],[16,20,22,16],[18,25,22,17]])
print(temp)

for day in temp:
        mean_temp.append(np.mean(day))

print (days[mean_temp.index(max(mean_temp))])
