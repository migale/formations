'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# sequence
sequence = "atg ccc AAA AAC Cat GGa Taa"
print ("Sequence : ", sequence)

# sequence en majuscule
sequpper = sequence.upper()
print ("Sequence en majuscule: ", sequpper)

# passage en liste
seqliste = sequence.split()
print ("SeqListe ", seqliste)

# longueur de la sequence
length = len (seqliste) * 3
print ("Longueur de la séquence:",length)

# premier codon
codon1 = seqliste[0]
print ("Premier codon:", codon1)

# dernier codon
codont = seqliste[len(seqliste)-1]
print ("Dernier codon:", codont)
