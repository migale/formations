'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Première solution
print("Je m'appelle [Prénom] [Nom].")

# Deuxième solution
print('Je m\'appelle [Prénom] [Nom].')

# Troisième solution
print('''Je m'appelle [Prénom] [Nom].''')
