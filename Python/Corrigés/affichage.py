'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

print("{:^11s}".format("*"))
print("{:^11s}".format("*"*3))
print("{:^11s}".format("*"*5))
print("{:^11s}".format("*"*7))
print("{:^11s}".format("*"*9))
