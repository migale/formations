'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

def reverse2(seq):
    list_seq = list(seq)
    list_seq.reverse()
    return "".join(list_seq)

def reverse1(seq) :
    return seq[::-1]

def reverse(seq):
    rev = ""

    for i in range(len(seq)) :

        rev += seq[len(seq)-1-i]

    return rev

def complement(seq) :

    comp = ""

    for nucleotide in seq :

        nucleotides = {"A" : "T", "T" : "A", "G" : "C", "C" : "G"}

        comp += nucleotides[nucleotide]

    return comp

# Sequence initiale
sequence = "ATGATGCAGCT"
print ("Sequence initiale : ", sequence)

seqreverse = reverse(sequence)
print ("Sequence reversee : ", seqreverse)

seqcomplement = complement(seqreverse)
print ("Sequence finale : ", seqcomplement)
