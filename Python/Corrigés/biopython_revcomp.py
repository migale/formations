'''
Created on March 2019

@author: sandra.derozier@inra.fr & olivier.inizan@inra.fr
'''

# Import de Biopython
from Bio.Seq import Seq

# Sequence initiale
sequence = "ATGATGCAGCT"
print(sequence)

# Creation d'un objet Sequence
seq = Seq(sequence)
print(seq)

# Sequence complementaire
seqreverse = seq.complement()
print(seqreverse)

# Sequence complementaire inverse
seqcomplement = seq.reverse_complement()
print(seqcomplement)
