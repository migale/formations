# Decktape

To convert an online HTML presentation and have it exported into the working directory under FormationPython2019.pdf filename:

```bash
docker run -t -v `pwd`:/slides astefanutti/decktape http://genome.jouy.inra.fr/~sderozier/formations/Python/Supports/Python2019.html FormationPython2019.pdf
```

Cf. documentation : https://github.com/astefanutti/decktape.