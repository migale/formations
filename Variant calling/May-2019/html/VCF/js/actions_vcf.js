$(function(){
	
	$('#mainTable').editableTableWidget();
	$('#mainTable td').css("border-width:50px");
	window.prettyPrint && prettyPrint();
	
	$("#example").click(function(){
		$('#mainTable tr:last').after('<tr><td>ref</td><td>12</td><td>.</td><td>T</td><td>C</td><td>147.5</td><td>.</td><td></td><td>GT:AD:DP:GQ:PL</td><td><span class="gt">1/1</span>:<span class="ad">0,3</span>:<span class="dp">3</span>:<span class="gq">9.1</span>:<span class="pl">95,25,0</td><td><span class="gt">0/0</span>:<span class="ad">2,0</span>:<span class="dp">2</span>:<span class="gq">5.4</span>:<span class="pl">0,5,42</span></td></tr>');
		$('#mainTable tr:last').after('<tr><td>ref</td><td>16</td><td>.</td><td>A</td><td>G</td><td>189.8</td><td>.</td><td></td><td>GT:AD:DP:GQ:PL</td><td><span class="gt">1/1</span>:<span class="ad">0,3</span>:<span class="dp">3</span>:<span class="gq">9.1</span>:<span class="pl">95,25,0</td><td><span class="gt">1/1</span>:<span class="ad">0,2</span>:<span class="dp">2</span>:<span class="gq">4.4</span>:<span class="pl">24,5,0</span></td></tr>');
		$('#mainTable tr:last').after('<tr><td>ref</td><td>28</td><td>.</td><td>G</td><td>C</td><td>105.5</td><td>.</td><td></td><td>GT:AD:DP:GQ:PL</td><td><span class="gt">0/0</span>:<span class="ad">2,0</span>:<span class="dp">2</span>:<span class="gq">5.4</span>:<span class="pl">0,5,42</td><td><span class="gt">0/1</span>:<span class="ad">3,2</span>:<span class="dp">5</span>:<span class="gq">14.4</span>:<span class="pl">10,0,45</span></td></tr>');
		$('#mainTable tr:last').after('<tr><td>ref</td><td>37</td><td>.</td><td>C</td><td>T,A</td><td>95.4</td><td>.</td><td></td><td>GT:AD:DP:GQ:PL</td><td><span class="gt">1/1</span>:<span class="ad">0,2,0</span>:<span class="dp">2</span>:<span class="gq">5.4</span>:<span class="pl">54,45,0,54,85,90</span></td><td><span class="gt">2/2</span>:<span class="ad">0,0,3</span>:<span class="dp">3</span>:<span class="gq">41.4</span>:<span class="pl">84,90,75,45,15,0</span></td></tr>');
		$('#mainTable tr:last').after('<tr><td>ref</td><td>45</td><td>.</td><td>T</td><td>G</td><td>24.5</td><td>.</td><td></td><td>GT:AD:DP:GQ:PL</td><td>./.</td><td><span class="gt">1/1</span>:<span class="ad">0,2</span>:<span class="dp">2</span>:<span class="gq">10.8</span>:<span class="pl">34,8,0</span></td></tr>');
		$(this).attr("disabled", true);
		//$('#boutons').slideToggle("fast");
		$(this).prop('disabled', true);
	});

	$("#response").click(function(){
		$('#mainTable thead').html("<tr><th>CHROM</th><th>POS</th><th>ID</th><th>REF</th><th>ALT</th><th>QUAL</th><th>FILTER</th><th>INFO</th><th>FORMAT</th><th>s1 GENOTYPE FIELDS</th><th>s2 GENOTYPE FIELDS</th></tr>");
		$('#prop').remove();
		$('#boutons').slideToggle("fast");
		$(this).prop('disabled', true);
		$("#devinette").remove();
	});
		
	$("#flag").click(function() {
		var href = $(this).attr('href');
		if (href.indexOf('http://') != -1 || href.indexOf('https://') != -1) {
			var host = href.substr(href.indexOf(':')+3);
			if (host.indexOf('/') != -1) {
				host = host.substring(0, host.indexOf('/'));
			}
			if (host != window.location.host) {
				window.open(href);
				return false;
			}
		}
	});

	var genotype_table = '<div style="margin-top:20px">\
				<table id="mainTable" class="table table-striped">\
					<tbody>\
							<tr><td></td><td>REF</td><td>ALT 1</td><td>ALT 2</td></tr>\
							<tr><td>REF</td><td>0/0</td><td>0/1</td><td>0/2</td></tr>\
							<tr><td>ALT 1</td><td></td><td>1/1</td><td>1/2</td></tr>\
							<tr><td>ALT 2</td><td></td><td></td><td>2/2</td></tr>\
					</tbody>\
				</table>\
			</div>';

	$("#GT").click(function(){
		$('#basicModal').modal('show');
		$('.modal-body').html(genotype_table);
	});
	
	$("#gVCF").click(function(){
		
		$('#boutons').append('<img id="theImg" src="img/gVCF.png" />')
	});
	
	$("#AD").click(function(){
		$('#basicModal').modal('show');
		$('.modal-body').html("REF depth, ALT 1 depth, ALT2 depth ...");
	});
	
	$( "#gt,#ad,#gq,#pl,#dp" ).hover(function() {
		cl = $(this).attr("id");
		$( "."+cl ).each(function() {
			var fontSize = parseInt($(this).css("font-size"));
			fontSize = fontSize + 10 + "px";
			$(this).css({'font-size':fontSize});
		});
	},
	function(){
		cl = $(this).attr("id");
		$( "."+cl ).each(function() {
			var fontSize = parseInt($(this).css("font-size"));
			fontSize = fontSize - 10 + "px";
			$(this).css({'font-size':fontSize});
		});
	});
	
});

