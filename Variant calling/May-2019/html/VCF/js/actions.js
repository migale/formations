$(function(){
	
	$('#mainTable').editableTableWidget();
	$('#mainTable td').css("border-width:50px");
	window.prettyPrint && prettyPrint();
	
	$("#example").click(function(){
		$('#mainTable tr:last').after('<tr><td>read1/1</td><td>99</td><td>Ref</td><td>7</td><td>37</td><td>2S8M2I4M1D3M</td><td>=</td><td>37</td><td>39</td><td>AATTAGATAAAGGATACTG</td><td>GHHGCGGFE?A8C@BABDD</td><td></td></tr>');
		$('#mainTable tr:last').after('<tr><td>read2</td><td>0</td><td>Ref</td><td>16</td><td>30</td><td>6M14N5M</td><td>*</td><td>0</td><td>0</td><td>ATAGCTTCAGC</td><td>B??EHHAC@HE</td><td>NM:i:0&nbsp;&nbsp;&nbsp;&nbsp;MD:Z:11</td></tr>');
		$('#mainTable tr:last').after('<tr><td>read1/2</td><td>147</td><td>Ref</td><td>37</td><td>30</td><td>9M</td><td>=</td><td>7</td><td>-39</td><td>CAGCGGCAT</td><td>E?A8C@BAB</td><td>NM:i:1&nbsp;&nbsp;&nbsp;&nbsp;MD:Z:5C3</td></tr>');
		$(this).attr("disabled", true);
	});

	$("#response").click(function(){
		$('#mainTable thead').html("<tr><th>QNAME</th><th>FLAG</th><th>RNAME</th><th>POS</th><th>MAPQ</th><th>CIGAR</th><th>MRNM</th><th>MPOS</th><th>ISIZE</th><th>SEQQuery</th><th>QUAL</th><th>[&#60;TAG&#62;:&#60;VTYPE&#62;:&#60;VALUE&#62;]</th></tr>");
		//$('#prop').remove();
		$('#boutons').slideToggle("fast");
		$(this).prop('disabled', true);
	});
	
	$("#flag").click(function() {
		var href = $(this).attr('href');
		if (href.indexOf('http://') != -1 || href.indexOf('https://') != -1) {
			var host = href.substr(href.indexOf(':')+3);
			if (host.indexOf('/') != -1) {
				host = host.substring(0, host.indexOf('/'));
			}
			if (host != window.location.host) {
				window.open(href);
				return false;
			}
		}
	});

	$("#cigar").click(function(){
		$('#basicModal').modal('show');
		$('.modal-body').html('Cigar informations');
		$('.modal-body').prepend('<img id="theImg" src="img/cigar.png" />')
	});
	
	$("#nm").click(function(){
		$(this).parent().append("<i style='padding-left: 1em;font-size: 0.8em;'>Number of mismatches (NM:i:0)</i>");
	});

	$("#md").click(function(){
		$(this).parent().append("<i style='padding-left: 1em;font-size: 0.8em;'>String for mismatching positions (MD:36A4T2)</i>");
	});

	$("#rg").click(function(){
		$(this).parent().append("<i style='padding-left: 1em;font-size: 0.8em;'>Read Group ID (RG:Z:control)</i>");
	});

	$("#x0").click(function(){
		$(this).parent().append("<i style='padding-left: 1em;font-size: 0.8em;'>Number of best hits (X0:i:10)</i>");
	});

	$("#x1").click(function(){
		$(this).parent().append("<i style='padding-left: 1em;font-size: 0.8em;'>Number of sub-optimal hits (X1:i:25)</i>");
	});

	$("#xa").click(function(){
		$(this).parent().append("<i style='padding-left: 1em;font-size: 0.8em;'>Alternative hits; format: (chr,pos,CIGAR,NM;)*</i>");
	});
});
