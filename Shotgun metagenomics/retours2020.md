# Retours session 16-17 septembre 2020

## Time code

Mardi :

* 9h30 - 10h10 : Intro générale, présentations des participants, intro métagénomiques, objectifs
* 10h10 - 11h15 : Rappels sur l'infra
* 11h15 - 11h30 : Rappels NGS
* *11h30 - 11h40 : PAUSE*
* 11h40 - 13h05 : Présentation des données, contrôle qualité
* *13h05 - 14h10 : PAUSE*
* 14h10 - 15h05 : Assignation taxo
* 15h05 - 15h35 : FASTP
* 15h35 - 15h45 : SortMeRNA (pas de TP)
* 15h45 - 15h55 : Contaminants (pas de TP)
* 16h15 - 16h40 : KHMER
* 16h40 - 17h25 : SPADES (avec lien symbolique depuis les reads de la correction)

Mercredi :

* 9h05 - 9h20 : Rappels et échanges
* 9h20 - 10h50 : Résultats de l'assemblage et QUAST
* *10h50 - 11h10 : PAUSE*
* 11h10 - 10h50 : Mapping
* 10h50 - 12h25 : METABAT
* 12h25 - 13h00 : CheckM
* *13h00 - 14h05 : PAUSE*
* 14h05 - 14h20 : Résultats CheckM + metaQUAST sur les bins 
* 14h20 - 15h50 : Annotation prokka et eggNOG
* 15h50 - 16h10 : Comptage
* 16h10 - 16h15 : Résumé de la journée
* *16h15 - 16h35 : PAUSE*
* 16h35 - 16h45 : Automatisation
* 16h45 - 16h55 : Conclusion
* 16h55 - 17h30 : Discussion

## Retours participants :

* Développer l'exploitation des données fonctionelles par un croisement entre les comptages et annotations eggNOG (Stéphane)
* Besoin d'aide et d'avis sur la rédaction de DataPaper &rarr; Réorienté vers la DipSO (Marion)
* Ajout d'un jeu de donnée qui ne marche pas ainsi que seuil minimal pour GO/NOGO (Sebastien ?)
*Approfondir la partie l'assemblage (Sebastien)
* Besoin d'un peu plus de schémas
* Support du TP très apprécié ++

## Notes Cédric :

* Vocabulaire infra (thread, slot, coeur, ...) dans les slides ? (déja présent dans le tuto)
* Ajouts sur les bases internationales, ENA, SRA-toolkit, ...
* Présenter les formats SAM/BAM (de la même manière que FASTQ au début)
* On a fait les paste avec qlogin. C'est plus simple et ça permet de faire un rappel sur qlogin
* On a utiliser qacct sur les jobs de spades (qui ont tourné pendant la nuit) et ça permet aussi de faire des rappels sur le monitoring
* Prévoir schéma pour N50 
* Erreurs MetaGeneMark avec quast à investiger
* METABAT pas reproductible. Fixer seed ou tester GroopM
* Lancer metaQUAST sur les bins reconstruits
* Suppléments sur les COG, eggNOG, iPath, ...
* 