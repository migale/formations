<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <title>First steps with NGS data</title>
    <meta charset="utf-8" />
    <meta name="author" content="Valentin Loux - Olivier Rué" />
    <link href="slides_OR_files/remark-css/default.css" rel="stylesheet" />
    <link href="slides_OR_files/remark-css/default-fonts.css" rel="stylesheet" />
    <link href="slides_OR_files/font-awesome/css/fontawesome-all.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/styles.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# First steps with NGS data
## DUBii - Module 5
### Valentin Loux - Olivier Rué
### 2020/03/09

---






# Program

- Introduction (5 min)
- Get data from public resources (30 min)
- FASTQ format
- Quality control (45 min)
- Cleaning of reads (30 min)
- Mapping of reads (30 min)
- FASTA format
- SAM format

&lt;!-- 
Total : 2h 35

--&gt;
---
# Preparation of your working directory

## Instruction

- Go in your home directory
- Create a directory called MODULE5_TP1
- Move in this directory

## Correction


```bash
cd ~/
mkdir MODULE5_TP1
cd MODULE5_TP1
```

```
## mkdir: impossible de créer le répertoire «MODULE5_TP1»: Le fichier existe
```

---
class: heading-slide, middle, center
# The Data

---

# What is data

## Definition

- `Data` is &lt;i&gt;a symbolic representation of information&lt;/i&gt;
- `Data` is stored in files whose format allows an easy way to access and manipulate
- `Data` represent the knowledge at a given time.

## Properties

- The same information may be represented in different formats
- The content depends on technologies

&lt;div class="alert comment"&gt;Understanding data formats, what information is encoded in each, and when it
is appropriate to use one format over another is an essential skill of a bioinfor-
matician.&lt;/div&gt;

---

# Storage and accessibility of data

## Primary analyses

- Sequencing data are often available. It is a prerequite for publishing results.

## Expertized data

- Some repositories exist, often dedicated to a particular organism

---

# DNA sequences resources

The International Nucleotide Sequence Database Collaboration (INSDC) is a long-standing foundational initiative that operates between DDBJ, EMBL-EBI and NCBI. INSDC covers the spectrum of data raw reads, through alignments and assemblies to functional annotation, enriched with contextual information relating to samples and experimental configurations.

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/public_resources.png" alt="INDSC resources" width="70%" /&gt;
&lt;p class="caption"&gt;INDSC resources&lt;/p&gt;
&lt;/div&gt;

---
# DNA sequences resources

The member organizations of this collaboration are:
- NCBI: National Center for Biotechnology Information
- EMBL: European Molecular Biology Laboratory
- DDBJ: DNA Data Bank of Japan

The INSDC has set up rules on the types of data that will be mirrored. The most important
of these from a bioinformatician’s perspective are:
- GenBank/Ebi ENA contains all annotated and identified DNA sequence information
- SRA [NCBI Sequence Reads Archive](https://trace.ncbi.nlm.nih.gov/Traces/sra/) / ENA [European Nucleotide Archive](https://www.ebi.ac.uk/ena/browser/search): Short Read Archive contains measurements from high throughput sequencing
experiments (raw data)
- UniProt: Universal Protein Resource is the most authoritative repository of protein
sequence data.
- Protein Data Bank (PDB) is the major repository of 3D structural information about
biological macromolecules (proteins and nucleic acids). PDB contains structures for a spectrum of biomolecules - from small bits of proteins/nucleic acids all the way to
complex molecular structures like ribosomes.

---
# DNA sequences resources

## List of most resources

Once a year the journal Nucleic Acids Research publishes its so-called “database issue”. Each
article of this issue of the journal will provide and overview and updates about a specific
database written by the maintainers of that resource.
- View the NAR: 2019 Database Issue.

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/NAR_db.png" alt="NAR 2019 database issue overview" width="50%" /&gt;
&lt;p class="caption"&gt;NAR 2019 database issue overview&lt;/p&gt;
&lt;/div&gt;

---
class: heading-slide, middle, center
# Get Data

---
# Get data

## Sequencing data

- Tools or API are offered to easily get data locally
- ENA: enaBrowserTools (command line, python, R)
- NCBI: sra-toolkit (command line, python, R)

## Data from FTP servers or HTTP

- wget / ftp command lines




---
# Get data

We will focus on data associated with this article:

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/MRA.01052-18.png" alt="Allué-Guardia A et al. 2019. Microbiol Resour Announc 8:e01052-18. https://doi.org/10.1128/MRA.01052-18." width="70%" /&gt;
&lt;p class="caption"&gt;Allué-Guardia A et al. 2019. Microbiol Resour Announc 8:e01052-18. https://doi.org/10.1128/MRA.01052-18.&lt;/p&gt;
&lt;/div&gt;

- Short reads from Illumina

---
# TP1 : Get Data

## Instruction

- In the "Data availability" section, extract the accession for Illumina data : SRX4909245
- Explore [SRA](https://www.ncbi.nlm.nih.gov/sra/SRX4909245) and [ENA](https://www.ebi.ac.uk/ena/browser/view/SRX4909245)

Get the data by the method of your choice: via the web browser, &lt;code&gt;wget&lt;/code&gt; or &lt;code&gt;fastq-dump&lt;/code&gt;

--

## Correction


```bash
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR808/003/SRR8082143/SRR8082143_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR808/003/SRR8082143/SRR8082143_2.fastq.gz
```
or

```bash
# conda env list |grep sra-tools
source activate sra-tools-2.10.0
fastq-dump --split-3 SRR8082143 --gzip
conda deactivate
```


---
class: heading-slide, middle, center
# File formats

---

# FASTA

The FASTA format is used to represent sequence information. The format is very simple:
- A &lt;code&gt;&gt;&lt;/code&gt; symbol on the FASTA header line indicates a fasta record start.
- A string of letters called the sequence id may follow the &lt;code&gt;&gt;&lt;/code&gt; symbol.
- The header line may contain an arbitrary amount of text (including spaces) on the
same line.
- Subsequent lines contain the sequence.

--

&lt;i&gt;Example&lt;/i&gt;


```bash
&gt;foo
ATGCC
&gt;bar other optional text could go here
CCGTA
&gt;bidou
ACTGCAGT
TTCGN
&gt;repeatmasker
ATGTGTcggggggATTTT
&gt;prot2; my_favourite_prot
MTSRRSVKSGPREVPRDEYEDLYYTPSSGMASP
```

---

# FASTA

The lack of a definition of the FASTA format and its apparent simplicity can be a source of
some of the most confounding errors in bioinformatics. Since the format appears so exceed-
ingly straightforward, software developers have been tacitly assuming that the properties
they are accustomed to are required by some standard - whereas no such thing exists. 

## Common problems

- Some tools need 60 characters per line
- Some tools ignore anything following the first space in the header line
- Some tools are very restrictive on the alphabet used
- Some tools require uppercase letters

---

# FASTA

## Good practices

The sequence lines should always wrap at the same width (with the exception of the
last line). Some tools will fail to operate correctly and may not even warn the users if
this condition is not satisfied. The following is technically a valid FASTA but it may
cause various subtle problems.


```bash
&gt;foo
ATGCATGCATGCATGCATGC
ATGCATGCA
TGATGCATGCATGCATGCA
```

should be reformated to


```bash
&gt;foo
ATGCATGCATGCATGCATGC
ATGCATGCATGATGCATGCA
TGCATGCA
```

&lt;i&gt;Can be easily to with seqkit tool&lt;/i&gt;


```bash
seqkit seq -w 60 seqs.fa &gt; seqs2.fa
```

---

# FASTA

Some data repositories will format FASTA headers to include structured information. Tools
may operate differently when this information is present in the FASTA header. Below is a list of the
recognized FASTA header formats.

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/FASTA_headers.png" alt="FASTA header examples" width="50%" /&gt;
&lt;p class="caption"&gt;FASTA header examples&lt;/p&gt;
&lt;/div&gt;

---

# FASTA

## Seqkit


```bash
[orue@migale work]$ seqkit seq -h
transform sequences (revserse, complement, extract ID...)

Usage:
  seqkit seq [flags]

Flags:
  -p, --complement                complement sequence (blank for Protein sequence)
      --dna2rna                   DNA to RNA
  -G, --gap-letters string        gap letters (default "- \t.")
  -h, --help                      help for seq
  -l, --lower-case                print sequences in lower case
  -M, --max-len int               only print sequences shorter than the maximum length (-1 for no limit) (default -1)
  -m, --min-len int               only print sequences longer than the minimum length (-1 for no limit) (default -1)
  -n, --name                      only print names
  -i, --only-id                   print ID instead of full head
  -q, --qual                      only print qualities
  -g, --remove-gaps               remove gaps
  -r, --reverse                   reverse sequence
      --rna2dna                   RNA to DNA
  -s, --seq                       only print sequences
  -u, --upper-case                print sequences in upper case
  -v, --validate-seq              validate bases according to the alphabet
  -V, --validate-seq-length int   length of sequence to validate (0 for whole seq) (default 10000)

Global Flags:
      --alphabet-guess-seq-length int   length of sequence prefix of the first FASTA record based on which seqkit guesses the sequence type (0 for whole seq) (default 10000)
      --id-ncbi                         FASTA head is NCBI-style, e.g. &gt;gi|110645304|ref|NC_002516.2| Pseud...
      --id-regexp string                regular expression for parsing ID (default "^([^\\s]+)\\s?")
  -w, --line-width int                  line width when outputing FASTA format (0 for no wrap) (default 60)
  -o, --out-file string                 out file ("-" for stdout, suffix .gz for gzipped out) (default "-")
      --quiet                           be quiet and do not show extra information
  -t, --seq-type string                 sequence type (dna|rna|protein|unlimit|auto) (for auto, it automatically detect by the first sequence) (default "auto")
  -j, --threads int                     number of CPUs. (default value: 1 for single-CPU PC, 2 for others) (default 2)

```

---
# Sequencing data - Glossary

.pull-left[
**Read** :  piece of sequenced DNA

**DNA fragment** = 1 or more reads depending on whether the sequencing is single end or paird-end

**Insert** = Fragment size

**Depth** = `\(N*L/G\)` 
N= number of reads, L = size, G : genome size

**Coverage** = % of genome covered
]
.pull-right[
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/se-pe.png" alt="Single-End , Paired-End" width="90%" /&gt;
&lt;p class="caption"&gt;Single-End , Paired-End&lt;/p&gt;
&lt;/div&gt;

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/fragment-insert.png" alt="Single-End , Paired-End" width="90%" /&gt;
&lt;p class="caption"&gt;Single-End , Paired-End&lt;/p&gt;
&lt;/div&gt;

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/depth-breadth.png" alt="Single-End , Paired-End" width="90%" /&gt;
&lt;p class="caption"&gt;Single-End , Paired-End&lt;/p&gt;
&lt;/div&gt;

]

---


# FASTQ

The FASTQ format is the de facto standard by which all sequencing instruments represent
data. It may be thought of as a variant of the FASTA format that allows it to associate a
quality measure to each sequence base, FASTA with QUALITIES.

In simpler terms, it is a format where for every base, we associate a reliability measure:
base is A and the probability that we are wrong is 1/1000.

---

# FASTQ

The FASTQ format consists of 4 sections:
1. A FASTA-like header, but instead of the &lt;code&gt;&gt;&lt;/code&gt; symbol it uses the &lt;code&gt;@&lt;/code&gt; symbol. This is followed
by an ID and more optional text, similar to the FASTA headers.
2. The second section contains the measured sequence (typically on a single line), but it
may be wrapped until the &lt;code&gt;+&lt;/code&gt; sign starts the next section.
3. The third section is marked by the &lt;code&gt;+&lt;/code&gt; sign and may be optionally followed by the same
sequence id and header as the first section
4. The last line encodes the quality values for the sequence in section 2, and must be of
the same length as section 2.

&lt;i&gt;Example&lt;/i&gt;


```bash
@SEQ_ID
GATTTGGGGTTCAAAGCAGTATCGATCAAATAGTAAATCCATTTGTTCAACTCACAGTTT
+
!''*((((***+))%%%++)(%%%%).1***-+*''))**55CCF&gt;&gt;&gt;&gt;&gt;&gt;CCCCCCC65
```

---

# FASTQ

The weird characters in the 4th section are the so called “encoded” numerical values.
In a nutshell, each character represents a numerical value: a so-called Phred score,
encoded via a single letter encoding.


```bash
!"#$%&amp;'()*+,-./0123456789:;&lt;=&gt;?@ABCDEFGHI
|    |    |    |    |    |    |    |    |
0....5...10...15...20...25...30...35...40
|    |    |    |    |    |    |    |    |
worst................................best
```

The quality values of the FASTQ files are on top. The numbers in the middle of the scale
from 0 to 40 are called Phred scores. The numbers represent the error probabilities in a
somewhat convoluted manner via the formula:

Error=10ˆ(-P/10) basically summarized as:

- P=0 means 1/1 (100% error)
- P=10 means 1/10 (10% error)
- P=20 means 1/100 (1% error)
- P=30 means 1/1000 (0.1% error)
- P=40 means 1/10000 (0.01% error)

---

# FASTQ

&lt;div class="alert comment"&gt;<i class="fas  fa-exclamation-circle "></i> There are different versions of the FASTQ encoding&lt;/div&gt;

There was a time when instrumentation makers could not decide at what
character to start the scale. The current standard shown above is the so-called Sanger (+33)
format where the ASCII codes are shifted by 33. There is the so-called +64 format that
starts close to where the other scale ends.

&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/qualityscore.png" alt="FASTQ encoding values" width="80%" /&gt;
&lt;p class="caption"&gt;FASTQ encoding values&lt;/p&gt;
&lt;/div&gt;

---

# FASTQ 


```bash
[orue@migale work]$ seqtk seq

Usage:   seqtk seq [options] &lt;in.fq&gt;|&lt;in.fa&gt;

Options: -q INT    mask bases with quality lower than INT [0]
         -X INT    mask bases with quality higher than INT [255]
         -n CHAR   masked bases converted to CHAR; 0 for lowercase [0]
         -l INT    number of residues per line; 0 for 2^32-1 [0]
         -Q INT    quality shift: ASCII-INT gives base quality [33]
         -s INT    random seed (effective with -f) [11]
         -f FLOAT  sample FLOAT fraction of sequences [1]
         -M FILE   mask regions in BED or name list FILE [null]
         -L INT    drop sequences with length shorter than INT [0]
         -F CHAR   fake FASTQ quality []
         -c        mask complement region (effective with -M)
         -r        reverse complement
         -A        force FASTA output (discard quality)
         -C        drop comments at the header lines
         -N        drop sequences containing ambiguous bases
         -1        output the 2n-1 reads only
         -2        output the 2n reads only
         -V        shift quality by '(-Q) - 33'
         -U        convert all bases to uppercases
         -S        strip of white spaces in sequences
```


```bash
seqtk seq -Q64 input.fq &gt; output.fq
```

---

# FASTQ - Header informations

Just as with FASTA headers, information is often encoded in the “free” text section of a
FASTQ file.

&lt;code&gt;@EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG&lt;/code&gt; contains the following information:

- &lt;code&gt;EAS139&lt;/code&gt;: the unique instrument name
- &lt;code&gt;136&lt;/code&gt;: the run id
- &lt;code&gt;FC706VJ&lt;/code&gt;: the flowcell id
- &lt;code&gt;2&lt;/code&gt;: flowcell lane
- &lt;code&gt;2104&lt;/code&gt;: tile number within the flowcell lane
- &lt;code&gt;15343&lt;/code&gt;: ‘x’-coordinate of the cluster within the tile
- &lt;code&gt;197393&lt;/code&gt;: ‘y’-coordinate of the cluster within the tile
- &lt;code&gt;1&lt;/code&gt;: the member of a pair, 1 or 2 (paired-end or mate-pair reads only)
- &lt;code&gt;Y&lt;/code&gt;: Y if the read is filtered, N otherwise
- &lt;code&gt;18&lt;/code&gt;: 0 when none of the control bits are on, otherwise it is an even number
- &lt;code&gt;ATCACG&lt;/code&gt;: index sequence

This information is specific to a particular instrument/vendor and may change with different
versions or releases of that instrument.
---

# Quality control

## Why do a QC of your read ?

Answer to (not always) simple question :
--

Do the generated sequences conform to the expected level of performance?
- Size
- Number of reads
- Quality
- Residual presence of adapters or indexes ?
- Techincal biases (expected and unexpected) ?

&lt;div class="alert comment"&gt;<i class="fas  fa-exclamation-circle "></i> It is the knowledge of what you expect that should guide you on what you are looking for&lt;/div&gt;

---
# QC : software

- [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)   &lt;a name=cite-fastqc&gt;&lt;/a&gt;([Andrews, 2010](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)) **Quality Control for FastQ files**
  - QC for (Illumina) FastQ files
  - Command line fastqc or graphical interface
  - Complete HTML report to spot problem originating from sequencer, library preparation, contamination
  - Summary graphs and tables to quickly assess your data
  
  
&lt;div class="figure" style="text-align: center"&gt;
&lt;img src="images/fastqc.png" alt="FastQC software" width="40%" /&gt;
&lt;p class="caption"&gt;FastQC software&lt;/p&gt;
&lt;/div&gt;
  
---

---
# TP2 - Quality Control

Run FastQC on each of the FASTQ files you previsouly downloaded and download the HTML files on your local computer to visualize them.


```bash
source activate fastqc-0.11.8 # load fastqc tool
mkdir FASTQC # create output directory
for i in *.gz ; do srun --cpus=8 --cpus-per-task=8 fastqc $i -t 8 -o FASTQC ; done
conda deactivate
```


```bash
scp login@core.cluster.france-bioinformatique.fr:/~/MODULE5_TP1/FASTQC/*.html .
```


---
FastQC : some metrics
Quality, GC, Adpatater 
---
# Read Cleaning
Objectives 
- filetr / clean
- remove adptaters

Tools :
- sickle, trimmomatic, …
- All in one : fastp
---
# Practical : read cleaning

---
# Practical : one report 
multiqQC

---
class: heading-slide, middle, center
# Mapping

---
# Mapping

- Map short reads to a reference genome is predict the locus where a read comes from.
- The result of a mapping is the list of the most probable regions with an associated probability.

--

&lt;div class="alert comment"&gt;<i class="fas  fa-exclamation-circle "></i> But what is a reference?&lt;/div&gt;

---
# Reference

---

...


---
# Alignment strategies


```bash
GAAGCTCTAGGATTACGATCTTGATCGCCGGGAAATTATGATCCTGACCTGAGTTTAAGGCATGGACCCATAA
                 ATCTTGATCGCCGAC----ATT              # GLOBAL
                 ATCTTGATCGCCGACATT                  # LOCAL, with soft clipping
```

## Global alignment

Global alignments, which attempt to align every residue in every sequence, are most useful when the sequences in the query set are similar and of roughly equal size. (This does not mean global alignments cannot start and/or end in gaps.) A general global alignment technique is the &lt;code&gt;Needleman–Wunsch algorithm&lt;/code&gt;, which is based on dynamic programming.

## Local alignment

Local alignments are more useful for dissimilar sequences that are suspected to contain regions of similarity or similar sequence motifs within their larger sequence context. The &lt;code&gt;Smith–Waterman algorithm&lt;/code&gt; is a general local alignment method based on the same dynamic programming scheme but with additional choices to start and end at any place.

---
# Alignment strategies

Seed-and-extend mappers are a class of read mappers that break down each read sequence into seeds (i.e., smaller segments) to ﬁnd locations in the reference genome that closely match the read

.pull-left[
1. First, the mapper obtains a read
2. Second, the mapper selects smaller DNA segments from the read to
serve as seeds
3. Third, the mapper indexes a data structure with each seed to
obtain a list of possible locations within the reference genome that could result in
a match
4. Fourth, for each possible location in the list, the mapper obtains the
corresponding DNA sequence from the reference genome
5. Fifth, the mapper aligns the read sequence to the reference sequence, using an expensive sequence
alignment (i.e., veriﬁcation) algorithm to determine the similarity between the read
sequence and the reference sequence.
]
.pull-right[
&lt;img src="images/seed_and_extend.png" width="90%" style="display: block; margin: auto;" /&gt;
]
---
# Mapping tools

&lt;img src="images/mapping_tools.png" width="70%" style="display: block; margin: auto;" /&gt;

- Short reads: BWA &lt;a name=cite-bwa&gt;&lt;/a&gt;([Li, 2013](#bib-bwa))/ BOWTIE 

---
# TP4 - Mapping

## Instruction

- Map the reads to the reference genome located at &lt;code&gt;&lt;/code&gt;

## Correction


```bash
source activate bwa
srun bwa index sequence.fasta
srun bwa mem sequence.fasta 
```


---
# SAM / BAM formats

The SAM/BAM formats are so-called Sequence Alignment Maps. These files typically represent
the results of aligning a FASTQ file to a reference FASTA file and describe the individual,
pairwise alignments that were found. Different algorithms may create different alignments
(and hence BAM files)

&lt;img src="images/SAM_format.jpg" width="70%" style="display: block; margin: auto;" /&gt;


---
# Samtools
Swiss-knife for operating of SAM/BAM format

samtools view
samtools flagstat

---
# What about Long Reads

---




# Bibliography

&lt;a name=bib-fastqc&gt;&lt;/a&gt;[Andrews, S.](#cite-fastqc) (2010). _FastQC A
Quality Control tool for High Throughput Sequence Data_. URL:
[http://www.bioinformatics.babraham.ac.uk/projects/fastqc/](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/).

&lt;a name=bib-bwa&gt;&lt;/a&gt;[Li, H.](#cite-bwa) (2013). "Aligning sequence
reads, clone sequences and assembly contigs with BWA-MEM". In: _arXiv
preprint arXiv:1303.3997_.
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="libs/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9"
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();</script>

<script>
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
