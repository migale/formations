---
title: "DUBii - Genomics"
author: "Valentin Loux - Olivier Rué"
subtitle: "Module 4"
css: css/styles.css
date: "2020/12/01"
output:
  html_document:
    self_contained: true
    number_sections: FALSE
    code_folding: "show"
    toc: true
    toc_depth: 2
    toc_float: true
    css: ['css/html_doc.css', 'https://use.fontawesome.com/releases/v5.0.9/css/all.css']
    includes:
      after_body: footer.html

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(eval=FALSE, echo =FALSE, cache = FALSE, message = FALSE, warning = FALSE, cache.lazy = FALSE,
                      fig.height = 3.5, fig.width = 10.5)
```

# Get data

- ENA
- TP

# Formats

- FASTA
- FASTQ

# Quality control

- FASTQC
- MULTIQC
- TP

# Cleaning

- FASTP
- TP

# Mapping

- BWA / BOWTIE
- TP

# Alignment formats

- SAM / BAM
- TP
# Long Reads



***

# References
<br>