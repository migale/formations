# Formation Métagénomique Juillet 2020

* Slides : [link](FROGS-2020/slides.html)
* TP : [link](FROGS-2020/document.html)

# Formation NGS-Galaxy Octobre 2020

* Slides : [[link](NGS-Galaxy/slides.html)] [[pdf](NGS-Galaxy/slides.pdf)] 
* Hands-On : [[link](NGS-Galaxy/TPNGS-Galaxy.html)] [[pdf](NGS-Galaxy/TPNGS-Galaxy.pdf)]


