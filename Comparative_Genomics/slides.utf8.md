---
title: "Comparaison de génomes microbiens"
author: "Hélène Chiapello - Valentin Loux  <br/>(helene.chiapello|valentin.loux)@inrae.fr"

subtitle: "Cycle de formation à la bioinformatique par la pratique"
css: css/styles.css
date: "2021/05/28"
output:
    #html_document:
    #  self_contained: true
    #  number_sections: TRUE
    #  code_folding: "show"
    #  toc: true
    #  toc_depth: 2
    #  toc_float: true
    xaringan::moon_reader:
      nature:
        ratio: '16:9'

---
