# Bonnes pratiques pour une meilleure reproductibilité de ses analyses

## Objectifs pédagogiques

L'objectif de cette formation est d'initier les apprenants aux bonnes pratiques pour la reproductibilité des analyses.
Ils apprendront à rédiger des rapports d'analyse en R Markdown et à les déposer sur un dépôt GitHub.
Les principes FAIR seront également présentés. Durant la formation, nous utiliserons RStudio et GitHub

## Programme

* Principes et enjeux de la recherche reproductible
* Utilisation de GitHub
* Gestion des versions d'un document
* Rédaction de document computationnel
* Partage d'un rapport avec ses colaborateurs
* Principes FAIR

## Pré-requis

* aucun

-----

# Planning 

| Start | Stop  | Durée |                   Chapitre                    |
| :---: | :---: | :---: | :-------------------------------------------: |
|  9h   | 9h30  | 30min |            Acceuil / tour de table            |
| 9h30  | 10h15 | 45min | Intro : recherche repro, Pourquoi ? Comment ? |
| 10h15 | 10h30 | 15min |                    *PAUSE*                    |
| 10h30 | 12h15 | 1h45  |                  git/github                   |
| 12h15 | 13h45 | 1h30  |                    *REPAS*                    |
| 13h45 |  15h  | 1h15  |                  (r)markdown                  |
|  15h  | 15h15 | 15min |                    *PAUSE*                    |
| 15h15 |  16h  | 45min |          deployement html sur github          |
|  16h  | 16h45 | 45min |                  intro FAIR                   |
| 16h45 |  17h  | 15min |            Conclusion et questions            |
