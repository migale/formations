# Développement d'une application avec Shiny

## Objectifs pédagogiques

À l’issue de la formation, les stagiaires connaîtront les principes de bases et le fonctionnement du package "Shiny". Ils et elles seront capables de créer leurs premières applications web interactives à partir de scripts R. Les solutions de déploiement d'applications Shiny seront également abordées.

## Programme

* Principes généraux et fonctionnement d'une application Shiny
* Développent d'applications Shiny
* Déploiement d'applications Shiny

## Pré-requis

* Maitrise de R
